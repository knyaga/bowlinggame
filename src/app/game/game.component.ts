/**
 * Created by Otis on 24/01/17.
 */
import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {GameService} from "../service/game.service";
import {Game} from "../model/game.model";

@Component({
  moduleId: module.id,
  selector: 'app-game',
  templateUrl: 'game.component.html',
  styleUrls:['game.component.css']
})
export class GameComponent implements OnInit {

  gameForm: FormGroup;
  scores: Game[] = [];
  serverScores: any [] = [];
  isEndOfGame: boolean = false;

  constructor(private formBuilder: FormBuilder, private gameService: GameService){}

  ngOnInit(): void {
    this.gameForm = this.formBuilder.group({
      frames: this.formBuilder.group({
        first:['', Validators.required],
        second:['', Validators.required]
      })
    });
  }

  handleGameForm(formData) {
    // Limit number of play to 10
    if(this.scores.length >= 10) {
      this.isEndOfGame = true;
      return;
    }

    // Convert input values into number
    let first  = Number(formData.frames.first);
    let second = Number(formData.frames.second);

    // Validate total sum of frame <= 10 pins
    if(!((first + second) <= 10)) {
      return;
    }

    /** Determine frame type: STRIKE/SPARE/OPEN **/
    let type = (first == 0 && second == 0) ? "OPEN" :
      (first % 10 == 0) ? "STRIKE" : ((first + second) % 10 ==0 ) ? "SPARE" :  "OPEN";

    this.scores.push(new Game(first, second, type));

    this.submitResultToServer(this.scores);
  }

  /**
   * Process frame scores
   * @param scores
   */
  submitResultToServer(scores) {
    this.gameService.calculateScore(scores).subscribe(res => {
      console.log(res);
      this.serverScores = res.frames;
    });
  }
}
