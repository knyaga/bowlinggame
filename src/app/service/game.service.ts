/**
 * Created by Otis on 25/01/17.
 */
import { Injectable } from '@angular/core';
import {Http, Headers} from "@angular/http";
import {Game} from "../model/game.model";
import 'rxjs/add/operator/map';

const HEADER = { headers: new Headers({ 'Content-Type': 'application/json' }) };
const BASE_URL = "http://localhost:3000/score";

@Injectable()
export class GameService {

  constructor(private http: Http){}

  /**
   * Submit frame score to server
   * @param game
   * @returns {Observable<R>}
   */
  calculateScore(game: Game) {
    return this.http.post(`${BASE_URL}`, game, HEADER)
      .map(res => res.json());
  }

}
