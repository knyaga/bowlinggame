import { BowlingGamePage } from './app.po';

describe('bowling-game App', function() {
  let page: BowlingGamePage;

  beforeEach(() => {
    page = new BowlingGamePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
