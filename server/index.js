/**
 * Created by Otis on 25/01/17.
 */
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

var PORT = process.env.PORT || 3000;

/** Json middleware*/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

/*** CORS middleware **/
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.post('/score', function(req,res) {
  /** Fetch sent frames from client*/
  var frames = req.body;
  /** Initial sum value */
  var sum = 0;
  var total = 0;

  /**
   * Loop through sent frames to determine type: STRIKE/SPARE/OPEN
   */
  for(var i = 0; i < frames.length; i++)
  {
    if(frames[i].type === "STRIKE") {
      sum += 10;  // 10 pins pinned down
      if(i+1 < frames.length) // check whether i+1 is within array boundaries
        sum += frames[i+1].first + frames[i+1].second; // bonus
    }
    else if(frames[i].type === "SPARE") {
      sum += 10; // 10 pins pinned down
      if(i+1 < frames.length) // check whether i+1 is within array boundaries
        sum += frames[i+1].first // bonus
    }
    else
    {
      sum += frames[i].first + frames[i].second;
    }

    frames[i].sum = sum;
    sum = 0;
  }

  res.json({frames: frames});
});

app.listen(PORT, function(){
  console.log("Server started on localhost: " + PORT);
});
